<!DOCTYPE html>
<html>
<head>
	<title>PHP OOP | Activity</title>
</head>
<body>

	<ul>
		<li>
			<p>
				<?php echo $newProduct->printDetails(); ?>
				<br>
				Product name: <?php echo $newProduct->getName();?>
				<br>
				Price: <?php echo $newProduct->getPrice(); ?>
				<br>
				Category: <?php echo $newProduct->getCategory();?>
			</p>
		</li>
		<li>
			<p>
				<?php echo $newMobile->printDetails(); ?>
				<br>
				Mobile: <?php echo $newMobile->getName(); ?>
				<br>
				Mobile: <?php echo $newMobile->getPrice(); ?>
				<br>
				Mobile: <?php echo $newMobile->getCategory(); ?>
			</p>
		</li>
		<li>Print another sub product here:</li>

	</ul>

</body>
</html>