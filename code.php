<?php

/*
	Instructions:

	1. (Base class) Create a product class with five properties: name, price, short description, category, stock nos.

	2. Create two child classes for Product Class: Mobile and Computer

	3. Create a method called printDetails in each of the classes and must have the following input:

	Product: "The product has a name of <productName> and its price is <productPrice>, and the stock no. is <product stock> "

	Mobile: "Our laster mobile is <mobile name> with a cheapest price of <mobilePrice>"

	Computer: "Our newest computer is <computerName> and its base price is <computer price>"

	4. On the Product class, create the ff getters and setters:

	price,
	stock number,
	category

	5. Kindly create an instance of the ff:

	Product:
		name: Xiaomi Mi Monitor
		price: 22000
		short description: good for gaming and for coding
		stock number: 5
		category- peripherals

	Mobile:
		name - Xiaomi Redmi Note 10 pro
		price - 13590
		short description - latest xiaomi phone ever made
		stock no - 10
		category - mobiles and electronics

	Computer:
		name - HP Business Laptop
		price - 29000
		short description - HP Laptop only made for business
		stock no - 10
		category - laptops and computers

	6. Display the details of an instance of a class to the ff:

		Product - display it on the first list item of the ul tag in the index.php
		Mobile - display it on the second list item of the ul tag in the index.php
		Computer - display it on the last list item of the ul tag in the index.php

	7. Kindly update the ff. details of the objects:
		Product object - update it stock number from 5 to 3
		Mobile object - update the stock no. from 10 to 5
		Computer object - update the category from 'laptops and computers'  to 'laptops, computers and electronics'

*/

class Product {
	public $name;
	public $price;
	public $shortDesc;
	public $category;
	public $stockNo;

	public function __construct($nameValue, $priceValue, $shortDescValue, $catValue, $stockValue){
		$this->name = $nameValue;
		$this->price = $priceValue;
		$this->shortDesc= $shortDescValue;
		$this->category = $catValue;
		$this->stockNo = $stockValue;
	}

	public function printDetails(){
		return "The product has a name of $this->name and its price is $this->price, and the stock no is $this->stockNo";
	}

	public function getPrice(){
		return $this->price;
	}
	public function getCategory(){
		return $this->category;
	}
	public function getStock(){
		return $this->stockNo;
	}

	public function setPrice($priceValue){
		$this->price = $priceValue;
	}

	public function setCategory($catValue){
		$this->category = $catValue;
	}

	public function setStock($stockValue){
		$this->stock = $stockValue;
	}
}


class Mobile extends Product
{
		public function printDetails(){
			return "Our latest mobile is $this->name with a cheapest price of $this->price";
		}
	}
	
	class Computer extends Product
	{
		public function printDetails(){
		return "Our newest computer is $this->name and its base price is $this->price";
	}
}
?>
